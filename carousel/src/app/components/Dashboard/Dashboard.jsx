import React, { Component } from 'react';
import { getJSON } from '../../utils/fetch';
import CreditScoreSlide from '../CreditScore/CreditScoreSlide';
import bemHelper from '../../utils/bem';
import data from '../../utils/mockData';

import './dashboard.scss';

const cn = bemHelper({ block: 'dashboard' });

export default class Dashboard extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      min: 0,
      max: 0,
      value: 0,
      bandDescription: '',
      loading: true
    };
    
  }

  componentDidMount() {
    this.loadData();
  }

  loadData = () => {
    const {
      creditReportInfo: {
        score,
        equifaxScoreBandDescription,
        minScoreValue,
        maxScoreValue
      } = {}
    } = data;
    this.setState({
      min: minScoreValue,
      max: maxScoreValue,
      value: score,
      bandDescription: equifaxScoreBandDescription,
      loading: false
    });
  };

  render() {
    const { loading, min, max, value, bandDescription } = this.state;
    if (loading) return null;
    return (
      <div className={cn('container')}>
        <div className={cn('carousel')}>
          <div className={cn('blurred-bg')} />
          <CreditScoreSlide
            className={cn('score')}
            min={min}
            max={max}
            value={value}
            bandDescription={bandDescription}
          />
        </div>
      </div>
    )
  }
}
