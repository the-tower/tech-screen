import React from 'react';
import bemHelper from '../../utils/bem';
import './creditScore.scss';

const cn = bemHelper({ block: 'score'});

export default ({
  min,
  max,
  value,
  bandDescription
}) => (
  <div className={cn('info')}>
    <div className={cn('title')}>Your credit score is</div>
    <div className={cn('value')}>{value}</div>
    <div className={cn('scale')}>
      <span className={cn('out-of')}>out of </span>
      <span className={cn('max')}>{max}</span>
    </div>
    <div className={cn('band')}>{bandDescription}</div>
  </div>
);
