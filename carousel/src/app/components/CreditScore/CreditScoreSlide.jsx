import React, { Component } from 'react';

import PropTypes from 'prop-types';
import CreditScore from './CreditScore';
import ScoreBar from './ScoreBar';
import { ANIMATION_DURATION } from '../../../config/credit-score';
import { scoreInterval, easeFunc, linearTransform } from '../../utils/credit-score-animation';
import bemHelper from '../../utils/bem';
import './creditScoreSlide.scss';

const { number, string } = PropTypes;
const cn = bemHelper({ block: 'score-slide'});
const scaleBarValue = linearTransform([0, 1])([])

export default class CreditScoreSlide extends Component {

  constructor(props) {
    super(props);
    this.state = {
      startTS: 0,
      currentTS: 0
    };
    this.scaleBarValue = linearTransform([0, 1])([0, props.value / props.max]);
  }

  static propTypes = {
    min: number,
    max: number,
    value: number,
    bandDescription: string,
    className: string
  };

  componentDidMount() {
    const currentTS = Date.now();
    this.setState({
      startTS: currentTS,
      currentTS
    });
    this.normalizeTime = linearTransform([currentTS, currentTS + ANIMATION_DURATION])([0, 1]);
    setInterval(this.animationFrame, 20);
  }

  animationFrame = () => {
    const { startTS } = this.state;
    const currentTS = Date.now();
    this.setState({ currentTS });
    if ((currentTS - startTS) >= ANIMATION_DURATION) {
      this.animationInterval = clearInterval(this.animationInterval);
    }
  };

  getBarValue = () => {
    const { startTS, currentTS } = this.state;
    if (!this.normalizeTime) return 0;
    if ((currentTS - startTS) >= ANIMATION_DURATION) return this.scaleBarValue(1);
    const x = this.normalizeTime(currentTS);
    return this.scaleBarValue(easeFunc(x));
  };

  getScoreValue = () => {
    const { startTS, currentTS } = this.state;
    if (!this.normalizeTime) return 0;
    const x = this.normalizeTime(currentTS);
    if (x >= scoreInterval) return this.props.value;
    return Math.round(easeFunc(x) * this.props.value);
  }

  render() {
    const { min, max, value, bandDescription, className } = this.props;
    return (
      <div className={cn('container', null, className)}>
        <div className={cn('bar')}>
          <ScoreBar value={this.getBarValue()} />
        </div>
        <CreditScore
          min={min}
          max={max}
          value={this.getScoreValue()}
          bandDescription={bandDescription}
        />
      </div>
    );
  }
};
