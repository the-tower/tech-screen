import React from 'react';

const getCoordinates = normalizedAngle => {
  const rads = (normalizedAngle - 0.25) * Math.PI / 0.5;
  return {
    x: Math.cos(rads),
    y: Math.sin(rads)
  };
}

const getLargeArcFlag = normalizedAngle => normalizedAngle > 0.5 ? '1' : '0';

export default ({ value }) => {
  const { x, y } = getCoordinates(value);
  const largeArcFlag = getLargeArcFlag(value);
  return (
    <svg width="100%" height="100%" viewBox="-1.04 -1.04 2.08 2.08">
      <path d={`M 0 -1 A 1 1 0 ${largeArcFlag} 1 ${x} ${y}`} stroke="#A4E1EB" strokeWidth="0.017" fill="transparent" />
    </svg>
  );
}
