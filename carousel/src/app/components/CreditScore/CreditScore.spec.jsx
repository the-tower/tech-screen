import React from 'react';
import { mount } from 'enzyme';

import CreditScore from './CreditScore';

describe('<CreditScore />', () => {
  let wrapper;
  beforeAll(() => {
    wrapper = mount(
      <CreditScore
        min={0}
        max={700}
        value={514}
        bandDescription="Excellent"
      />
    );
  });
  it('renders title', () => {
    const title = wrapper.find('.score__title');
    expect(title).toHaveLength(1);
    expect(title.first().text()).toMatch(/your credit score is/i);
  });

  it('renders score value', () => {
    const value = wrapper.find('.score__value');
    expect(value).toHaveLength(1);
    expect(value.first().text()).toMatch(/514/);
  });

  it('renders scale', () => {
    const scale = wrapper.find('.score__scale');
    expect(scale).toHaveLength(1);
    expect(scale.first().text()).toMatch(/out of 700/i);
  });

  it('renders band description', () => {
    const band = wrapper.find('.score__band');
    expect(band).toHaveLength(1);
    expect(band.first().text()).toMatch(/excellent/i);
  });
});