import React from 'react';
import { mount } from 'enzyme';

import ScoreBar from './ScoreBar';

describe('<ScoreBar />', () => {

  const pathRegexp = /A 1 1 0 (0|1) 1 (-?\d*(?:\.\d+)?(?:e[+-]?\d+)?) (-?\d*(?:\.\d+)?(?:e[+-]?\d+)?)/i;
  
  it('renders 1/4 of circle', () => {
    const wrapper = mount(<ScoreBar value={0.25} />);
    const path = wrapper.find('path');
    expect(path).toHaveLength(1);
    const d = path.first().prop('d');
    expect(d).toBeTruthy();
    const pathParams = d.match(pathRegexp);
    expect(pathParams).toHaveLength(4);
    expect(pathParams[1]).toBe('0');
    expect(Number(pathParams[2])).toBeCloseTo(1);
    expect(Number(pathParams[3])).toBeCloseTo(0);
  });

  it('renders 1/2 of circle', () => {
    const wrapper = mount(<ScoreBar value={0.5} />);
    const path = wrapper.find('path');
    expect(path).toHaveLength(1);
    const d = path.first().prop('d');
    expect(d).toBeTruthy();
    const pathParams = d.match(pathRegexp);
    expect(pathParams).toHaveLength(4);
    expect(pathParams[1]).toBe('0');
    expect(Number(pathParams[2])).toBeCloseTo(0);
    expect(Number(pathParams[3])).toBeCloseTo(1);
  });

  it('renders 3/4 of circle', () => {
    const wrapper = mount(<ScoreBar value={0.75} />);
    const path = wrapper.find('path');
    expect(path).toHaveLength(1);
    const d = path.first().prop('d');
    expect(d).toBeTruthy();
    const pathParams = d.match(pathRegexp);
    expect(pathParams).toHaveLength(4);
    expect(pathParams[1]).toBe('1');
    expect(Number(pathParams[2])).toBeCloseTo(-1);
    expect(Number(pathParams[3])).toBeCloseTo(0);
  });

  it('renders full circle', () => {
    const wrapper = mount(<ScoreBar value={1} />);
    const path = wrapper.find('path');
    expect(path).toHaveLength(1);
    const d = path.first().prop('d');
    expect(d).toBeTruthy();
    const pathParams = d.match(pathRegexp);
    expect(pathParams).toHaveLength(4);
    expect(pathParams[1]).toBe('1');
    expect(Number(pathParams[2])).toBeCloseTo(0);
    expect(Number(pathParams[3])).toBeCloseTo(-1);
  });
});