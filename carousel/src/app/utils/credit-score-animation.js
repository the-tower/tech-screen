import {
  MAX_SCORE,
  BOUNCE_INTERVAL,
  BOUNCE_MAGNITUDE,
  BOUNCE_DECREASE_FACTOR,
  BOUNCES_NUM } from '../../config/credit-score';

export const scoreInterval = 1 - BOUNCE_INTERVAL;
const singleBounceInterval = BOUNCE_INTERVAL / BOUNCES_NUM;

export const easeFunc = x => {
  if (x <= scoreInterval) return x / scoreInterval;
  for (let i = 0; i < BOUNCES_NUM; i++) {
    const leftEdge = scoreInterval + i * singleBounceInterval;
    const rightEdge = scoreInterval + (i + 1) * singleBounceInterval;
    const decrease = i ? i * BOUNCE_DECREASE_FACTOR : 1;
    if (leftEdge < x && x <= rightEdge) return (
      1 - (BOUNCE_MAGNITUDE / decrease) * Math.sin((x - leftEdge) * Math.PI / singleBounceInterval)
    );
  }
  return 1;
}

export const linearTransform = ([fromLeft, fromRight]) => {
  const fromInterval = fromRight - fromLeft;
  return ([toLeft, toRight]) => {
    const toInterval = toRight - toLeft;
    return x => {
      const interval = x - fromLeft;
      return toLeft + interval * toInterval / fromInterval;
    };
  };
}
