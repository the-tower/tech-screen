import { easeFunc, linearTransform } from './credit-score-animation';
import {
  MAX_SCORE,
  BOUNCE_INTERVAL,
  BOUNCE_MAGNITUDE,
  BOUNCE_DECREASE_FACTOR,
  BOUNCES_NUM } from '../../config/credit-score';

describe('score animation easeFunc', () => {
  it('returns expected values', () => {
    expect(easeFunc(0)).toBe(0);
    const scoreInterval = 1 - BOUNCE_INTERVAL;
    expect(easeFunc(scoreInterval / 2)).toBeCloseTo(0.5);
    expect(easeFunc(scoreInterval)).toBeCloseTo(1);
    const highestBouncePoint = scoreInterval + (BOUNCE_INTERVAL / BOUNCES_NUM) / 2;
    expect(easeFunc(highestBouncePoint)).toBeCloseTo(1 - BOUNCE_MAGNITUDE);
    expect(easeFunc(1)).toBeCloseTo(1);
  });
});

describe('linear transformation function', () => {
  const transform = linearTransform([1000, 2000])([0, 1]);
  it('returns the expected values', () => {
    expect(transform(1000)).toBeCloseTo(0);
    expect(transform(1500)).toBeCloseTo(0.5);
    expect(transform(2000)).toBeCloseTo(1);
  });
});