
export const MAX_SCORE = 700;
// Normalized values
export const BOUNCE_INTERVAL = 0.4;
export const BOUNCE_MAGNITUDE = 0.25;
export const BOUNCE_DECREASE_FACTOR = 2;
export const BOUNCES_NUM = 3;

// ms values
export const ANIMATION_DURATION = 3000;
